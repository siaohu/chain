import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode:'hash',
  routes: [
    {
      path: '/',
      name: 'home',
      component: resolve => require(['../components/pages/Home.vue'], resolve)
    },
    {
      path: '/login',
      name: 'login',
      component: resolve => require(['../components/pages/Login.vue'], resolve)
    },
    {
      path: '/recruit',
      name: 'recruit',
      component: resolve => require(['../components/pages/Recruit.vue'], resolve)
    },
    {
      path: '/recruit_message',
      name: 'recruit',
      component: resolve => require(['../components/pages/MessageLogin.vue'], resolve)
    },
    {
      path: '/login_success',
      name: 'login_success',
      component: resolve => require(['../components/pages/LoginSuccess.vue'], resolve)
    },
    {
      path: '/amend_success',
      name: 'amend_success',
      component: resolve => require(['../components/pages/AmendSuccess.vue'], resolve)
    },
    {
      path: '/register',
      name: 'amend_success',
      component: resolve => require(['../components/pages/Register.vue'], resolve)
    },
    {
      path: '/find_password',
      name: 'amend_success',
      component: resolve => require(['../components/pages/FindPassword.vue'], resolve)
    },
    {
      path: '/certification',
      name: 'certification',
      component: resolve => require(['../components/pages/Certification.vue'], resolve)
    },
    {
      path: '/subsuccess',
      name: 'certification',
      component: resolve => require(['../components/pages/SubSuccess.vue'], resolve)
    },
    {
      path: '/newpassword',
      name: 'newpassword',
      component: resolve => require(['../components/pages/NewPassword.vue'], resolve)
    },
  ]
})
