import axios from 'axios'
import qs from 'qs'

const instance = axios.create({
  baseURL: '',
  headers:
    {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  transformRequest: [function (data, headers)
  {
    return qs.stringify({
      ...data,
      sys: 'admin',
      token: localStorage.token,
      systemVersion:7,
      pVersion:1,
      version:1,
      system:'win',
      channel : 1,
      mobileInfo:'rate-admin',
    })

  }]
})

//拦截请求后的状态
instance.interceptors.response.use(
  response => {
    return response.data.data
  },
  error => {
    return Promise.reject(error) // 返回接口返回的错误信息
  })

//评级项目列表
export const projectList = params => {
  return instance.post('/rate/admin/project_list',params)
}




// instance.interceptors.response.use(res => { // 拦截请求后的状态
//     if (res.data.ret === 1) {
//         return res.data.data
//     } else {
//         // return Promise.reject(res.msg)
//         // return res.data.msg
//     }
// })
